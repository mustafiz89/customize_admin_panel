<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

Class Administrator_model extends CI_Model{
    
    public function select_subcategory_by_id($category_id)
    {
        $this->db->select('*');
        $this->db->from('tbl_subcategory');
        $this->db->join('tbl_category','tbl_category.category_id=tbl_subcategory.category_id');
        $this->db->where('tbl_category.category_id',$category_id);
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
    }
    public function select_all_category()
    {
        $this->db->select('*');
        $this->db->from('tbl_category');
        $query_result=$this->db->get();
        $result=$query_result->result();
        return $result;
    }
}
?>