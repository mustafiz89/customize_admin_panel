<!DOCTYPE html>
<!-- Template Name: Rapido - Responsive Admin Template build with Twitter Bootstrap 3.x Version: 1.2 Author: ClipTheme -->
<!--[if IE 8]><html class="ie8" lang="en"><![endif]-->
<!--[if IE 9]><html class="ie9" lang="en"><![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- start: HEAD -->

    <!-- Mirrored from www.cliptheme.com/demo/rapido/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 20 Jan 2015 13:46:18 GMT -->
    <head>
        <title>Responsive Admin Template</title>
        <!-- start: META -->
        <meta charset="utf-8" />
        <!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-status-bar-style" content="black">
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- end: META -->
        <!-- start: MAIN CSS -->
        <link href='../../../fonts.googleapis.com/cssdb4d.css?family=Raleway:400,300,500,600,700,200,100,800' rel='stylesheet' type='text/css'>
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/iCheck/skins/all.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/animate.css/animate.min.css">
        <!-- end: MAIN CSS -->
        <!-- start: CSS REQUIRED FOR SUBVIEW CONTENTS -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/owl-carousel/owl-carousel/owl.carousel.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/owl-carousel/owl-carousel/owl.theme.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/owl-carousel/owl-carousel/owl.transitions.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/summernote/dist/summernote.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/fullcalendar/fullcalendar/fullcalendar.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/toastr/toastr.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-select/bootstrap-select.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/DataTables/media/css/DT_bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/bootstrap-daterangepicker/daterangepicker-bs3.css">
        <!-- end: CSS REQUIRED FOR THIS SUBVIEW CONTENTS-->
        <!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/weather-icons/css/weather-icons.min.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/plugins/nvd3/nv.d3.min.css">
        <!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
        <!-- start: CORE CSS -->
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/styles.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/styles-responsive.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/plugins.css">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/themes/theme-default.css" type="text/css" id="skin_color">
        <link rel="stylesheet" href="<?php echo base_url();?>assets/css/print.css" type="text/css" media="print"/>
        <!-- end: CORE CSS -->
        <link rel="shortcut icon" href="favicon.ico" />
    </head>
    <!-- end: HEAD -->
    <!-- start: BODY -->
    <body>
        <!-- start: SLIDING BAR (SB) -->
       
        <!-- end: SLIDING BAR -->
        <div class="main-wrapper">
            <!-- start: TOPBAR -->
            <header class="topbar navbar navbar-inverse navbar-fixed-top inner">
                <!-- start: TOPBAR CONTAINER -->
                <div class="container">
                    <div class="navbar-header">
                        <a class="sb-toggle-left hidden-md hidden-lg" href="#main-navbar">
                            <i class="fa fa-bars"></i>
                        </a>
                        <!-- start: LOGO -->
                        <a class="navbar-brand" href="index.html">
                            <img src="<?php echo base_url();?>assets/images/" alt="Admin Panel"/>
                        </a>
                        <!-- end: LOGO -->
                    </div>
                    <div class="topbar-tools">
                        <!-- start: TOP NAVIGATION MENU -->
                        <ul class="nav navbar-right">
                            <!-- start: USER DROPDOWN -->
                            <li class="dropdown current-user">
                                <a data-toggle="dropdown" data-hover="dropdown" class="dropdown-toggle" data-close-others="true" href="#">
                                    <span class="username hidden-xs"><?php echo $this->session->userdata('admin_name');?></span> <i class="fa fa-caret-down "></i>
                                </a>
                                <ul class="dropdown-menu dropdown-dark">
                                    <li>
                                        <a href="pages_user_profile.html">
                                            My Profile
                                        </a>
                                    </li>
                                   
                                    <li>
                                        <a href="<?php echo base_url();?>administrator/logout">
                                            Log Out
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- end: USER DROPDOWN -->

                        </ul>
                        <!-- end: TOP NAVIGATION MENU -->
                    </div>
                </div>
                <!-- end: TOPBAR CONTAINER -->
            </header>
            <!-- end: TOPBAR -->
            <!-- start: PAGESLIDE LEFT -->
            <a class="closedbar inner hidden-sm hidden-xs" href="#">
            </a>
            <nav id="pageslide-left" class="pageslide inner">
                <div class="navbar-content">
                    <!-- start: SIDEBAR -->
                    <div class="main-navigation left-wrapper transition-left">
                        <div class="navigation-toggler hidden-sm hidden-xs">
                            <a href="#main-navbar" class="sb-toggle-left">
                            </a>
                        </div>

                        <!-- start: MAIN NAVIGATION MENU -->
                        <ul class="main-navigation-menu">
                                    <li>
                                        <a href="<?php echo base_url();?>administrator/dashboard"><i class="fa fa-folder"></i><span class="title"> Dashboard </span></a>                                        
                                    </li>
                                    <li>
                                        <a href="<?php echo base_url();?>administrator/table"><i class="fa fa-chevron-circle-left"></i><span class="title"> Table </span></a>                                        
                                    </li>
                                    
                                    <li>
                                        <a href="<?php echo base_url();?>administrator/category"><i class="fa fa-circle"></i><span class="title"> Category </span></a>                                        
                                    </li>
                            <li>
                                <a href="javascript:void(0)"><i class="fa fa-desktop"></i> <span class="title"> Layouts </span><i class="icon-arrow"></i> </a>
                                <ul class="sub-menu">
                                    
                                    
                                    <li>
                                        <a href="javascript:;">
                                            Horizontal Menu <i class="icon-arrow"></i>
                                        </a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="layouts_horizontal_menu.html">
                                                    Horizontal Menu
                                                </a>
                                            </li>
                                            <li>
                                                <a href="layouts_horizontal_menu_fixed.html">
                                                    Horizontal Menu Fixed
                                                </a>
                                            </li>
                                            <li>
                                                <a href="layouts_horizontal_sidebar_menu.html">
                                                    Horizontal &amp; Sidebar Menu
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="layouts_sidebar_closed.html">
                                            <span class="title"> Sidebar Closed </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="layouts_sidebar_not_fixed.html">
                                            <span class="title"> Sidebar Not Fixed </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="layouts_boxed_layout.html">
                                            <span class="title"> Boxed Layout </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="layouts_footer_fixed.html">
                                            <span class="title"> Footer Fixed </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="layouts_single_page.html">
                                            <span class="title"> Single-Page Interface </span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><i class="fa fa-cogs"></i> <span class="title"> UI Lab </span><i class="icon-arrow"></i> </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="ui_elements.html">
                                            <span class="title"> Elements </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="ui_buttons.html">
                                            <span class="title"> Buttons </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="ui_icons.html">
                                            <span class="title"> Icons </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="ui_animations.html">
                                            <span class="title"> CSS3 Animation </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="ui_subview.html">
                                            <span class="title"> Subview </span> <span class="label partition-blue pull-right ">HOT</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="ui_modals.html">
                                            <span class="title"> Extended Modals </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="ui_tabs_accordions.html">
                                            <span class="title"> Tabs &amp; Accordions </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="ui_panels.html">
                                            <span class="title"> Panels </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="ui_notifications.html">
                                            <span class="title"> Notifications </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="ui_sliders.html">
                                            <span class="title"> Sliders </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="ui_treeview.html">
                                            <span class="title"> Treeview </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="ui_nestable.html">
                                            <span class="title"> Nestable List </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="ui_typography.html">
                                            <span class="title"> Typography </span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><i class="fa fa-th-large"></i> <span class="title"> Tables </span><i class="icon-arrow"></i> </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="table_basic.html">
                                            <span class="title">Basic Tables</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="table_responsive.html">
                                            <span class="title">Responsive Tables</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="table_data.html">
                                            <span class="title">Advanced Data Tables</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="table_export.html">
                                            <span class="title">Table Export</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><i class="fa fa-pencil-square-o"></i> <span class="title"> Forms </span><i class="icon-arrow"></i> </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="form_elements.html">
                                            <span class="title">Form Elements</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="form_wizard.html">
                                            <span class="title">Form Wizard</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="form_validation.html">
                                            <span class="title">Form Validation</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="form_inline.html">
                                            <span class="title">Inline Editor</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="form_x_editable.html">
                                            <span class="title">Form X-editable</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="form_image_cropping.html">
                                            <span class="title">Image Cropping</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="form_multiple_upload.html">
                                            <span class="title">Multiple File Upload</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="form_dropzone.html">
                                            <span class="title">Dropzone File Upload</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><i class="fa fa-user"></i> <span class="title">Login</span><i class="icon-arrow"></i> </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="login_login.html">
                                            <span class="title"> Login Form </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="login_login6cf4.html?box=register">
                                            <span class="title"> Registration Form </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="login_login6255.html?box=forgot">
                                            <span class="title"> Forgot Password Form </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="login_lock_screen.html">
                                            <span class="title">Lock Screen</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><i class="fa fa-code"></i> <span class="title">Pages</span><i class="icon-arrow"></i> </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="pages_user_profile.html">
                                            <span class="title">User Profile</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="pages_invoice.html">
                                            <span class="title">Invoice</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="pages_gallery.html">
                                            <span class="title">Gallery</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="pages_timeline.html">
                                            <span class="title">Timeline</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="pages_calendar.html">
                                            <span class="title">Calendar</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="pages_messages.html">
                                            <span class="title">Messages</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="pages_blank_page.html">
                                            <span class="title">Blank Page</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript:void(0)"><i class="fa fa-cubes"></i> <span class="title">Utility</span><i class="icon-arrow"></i> </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="utility_faq.html">
                                            <span class="title">Faq</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="utility_search_result.html">
                                            <span class="title">Search Results </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="utility_404_example1.html">
                                            <span class="title">Error 404 Example 1</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="utility_404_example2.html">
                                            <span class="title">Error 404 Example 2</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="utility_404_example3.html">
                                            <span class="title">Error 404 Example 3</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="utility_500_example1.html">
                                            <span class="title">Error 500 Example 1</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="utility_500_example2.html">
                                            <span class="title">Error 500 Example 2</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="utility_pricing_table.html">
                                            <span class="title">Pricing Table</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="utility_coming_soon.html">
                                            <span class="title">Cooming Soon</span>
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript:;" class="active">
                                    <i class="fa fa-folder"></i> <span class="title"> 3 Level Menu </span> <i class="icon-arrow"></i>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="javascript:;">
                                            Item 1 <i class="icon-arrow"></i>
                                        </a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="#">
                                                    Sample Link 1
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    Sample Link 2
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    Sample Link 3
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            Item 1 <i class="icon-arrow"></i>
                                        </a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="#">
                                                    Sample Link 1
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    Sample Link 1
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    Sample Link 1
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">
                                            Item 3
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li>
                                <a href="javascript:;">
                                    <i class="fa fa-folder-open"></i> <span class="title"> 4 Level Menu </span><i class="icon-arrow"></i> <span class="arrow "></span>
                                </a>
                                <ul class="sub-menu">
                                    <li>
                                        <a href="javascript:;">
                                            Item 1 <i class="icon-arrow"></i>
                                        </a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="javascript:;">
                                                    Sample Link 1 <i class="icon-arrow"></i>
                                                </a>
                                                <ul class="sub-menu">
                                                    <li>
                                                        <a href="#"><i class="fa fa-times"></i> Sample Link 1</a>
                                                    </li>
                                                    <li>
                                                        <a href="#"><i class="fa fa-pencil"></i> Sample Link 1</a>
                                                    </li>
                                                    <li>
                                                        <a href="#"><i class="fa fa-edit"></i> Sample Link 1</a>
                                                    </li>
                                                </ul>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    Sample Link 1
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    Sample Link 2
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    Sample Link 3
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="javascript:;">
                                            Item 2 <i class="icon-arrow"></i>
                                        </a>
                                        <ul class="sub-menu">
                                            <li>
                                                <a href="#">
                                                    Sample Link 1
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    Sample Link 1
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    Sample Link 1
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                    <li>
                                        <a href="#">
                                            Item 3
                                        </a>
                                    </li>
                                </ul>
                            </li>

                        </ul>
                        <!-- end: MAIN NAVIGATION MENU -->
                    </div>
                    <!-- end: SIDEBAR -->
                </div>
                <div class="slide-tools">
                    <div class="col-xs-6 text-left no-padding">

                    </div>
                    <div class="col-xs-6 text-right no-padding">

                    </div>
                </div>
            </nav>
            <!-- end: PAGESLIDE LEFT -->
            <!-- start: PAGESLIDE RIGHT -->
            <div id="pageslide-right" class="pageslide slide-fixed inner">
                <div class="right-wrapper">
                    <ul class="nav nav-tabs nav-justified" id="sidebar-tab">
                        <li class="active">
                            <a href="#users" role="tab" data-toggle="tab"><i class="fa fa-users"></i></a>
                        </li>
                        <li>
                            <a href="#notifications" role="tab" data-toggle="tab"><i class="fa fa-bookmark "></i></a>
                        </li>
                        <li>
                            <a href="#settings" role="tab" data-toggle="tab"><i class="fa fa-gear"></i></a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="users">
                            <div class="users-list">
                                <h5 class="sidebar-title">On-line</h5>
                                <ul class="media-list">
                                    <li class="media">
                                        <a href="#">
                                            <i class="fa fa-circle status-online"></i>
                                            <img alt="..." src="<?php echo base_url();?>assets/images/avatar-2.jpg" class="media-object">
                                            <div class="media-body">
                                                <h4 class="media-heading">Nicole Bell</h4>
                                                <span> Content Designer </span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="media">
                                        <a href="#">
                                            <div class="user-label">
                                                <span class="label label-default">3</span>
                                            </div>
                                            <i class="fa fa-circle status-online"></i>
                                            <img alt="..." src="<?php echo base_url();?>assets/images/avatar-3.jpg" class="media-object">
                                            <div class="media-body">
                                                <h4 class="media-heading">Steven Thompson</h4>
                                                <span> Visual Designer </span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="media">
                                        <a href="#">
                                            <i class="fa fa-circle status-online"></i>
                                            <img alt="..." src="<?php echo base_url();?>assets/images/avatar-4.jpg" class="media-object">
                                            <div class="media-body">
                                                <h4 class="media-heading">Ella Patterson</h4>
                                                <span> Web Editor </span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="media">
                                        <a href="#">
                                            <i class="fa fa-circle status-online"></i>
                                            <img alt="..." src="<?php echo base_url();?>assets/images/avatar-5.jpg" class="media-object">
                                            <div class="media-body">
                                                <h4 class="media-heading">Kenneth Ross</h4>
                                                <span> Senior Designer </span>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                                <h5 class="sidebar-title">Off-line</h5>
                                <ul class="media-list">
                                    <li class="media">
                                        <a href="#">
                                            <img alt="..." src="<?php echo base_url();?>assets/images/avatar-6.jpg" class="media-object">
                                            <div class="media-body">
                                                <h4 class="media-heading">Nicole Bell</h4>
                                                <span> Content Designer </span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="media">
                                        <a href="#">
                                            <div class="user-label">
                                                <span class="label label-default">3</span>
                                            </div>
                                            <img alt="..." src="<?php echo base_url();?>assets/images/avatar-7.jpg" class="media-object">
                                            <div class="media-body">
                                                <h4 class="media-heading">Steven Thompson</h4>
                                                <span> Visual Designer </span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="media">
                                        <a href="#">
                                            <img alt="..." src="<?php echo base_url();?>assets/images/avatar-8.jpg" class="media-object">
                                            <div class="media-body">
                                                <h4 class="media-heading">Ella Patterson</h4>
                                                <span> Web Editor </span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="media">
                                        <a href="#">
                                            <img alt="..." src="<?php echo base_url();?>assets/images/avatar-9.jpg" class="media-object">
                                            <div class="media-body">
                                                <h4 class="media-heading">Kenneth Ross</h4>
                                                <span> Senior Designer </span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="media">
                                        <a href="#">
                                            <img alt="..." src="<?php echo base_url();?>assets/images/avatar-10.jpg" class="media-object">
                                            <div class="media-body">
                                                <h4 class="media-heading">Ella Patterson</h4>
                                                <span> Web Editor </span>
                                            </div>
                                        </a>
                                    </li>
                                    <li class="media">
                                        <a href="#">
                                            <img alt="..." src="<?php echo base_url();?>assets/images/avatar-5.jpg" class="media-object">
                                            <div class="media-body">
                                                <h4 class="media-heading">Kenneth Ross</h4>
                                                <span> Senior Designer </span>
                                            </div>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="user-chat">
                                <div class="sidebar-content">
                                    <a class="sidebar-back" href="#"><i class="fa fa-chevron-circle-left"></i> Back</a>
                                </div>
                                <div class="user-chat-form sidebar-content">
                                    <div class="input-group">
                                        <input type="text" placeholder="Type a message here..." class="form-control">
                                        <div class="input-group-btn">
                                            <button class="btn btn-blue no-radius" type="button">
                                                <i class="fa fa-chevron-right"></i>
                                            </button>
                                        </div>
                                    </div>
                                </div>
                                <ol class="discussion sidebar-content">
                                    <li class="other">
                                        <div class="avatar">
                                            <img src="<?php echo base_url();?>assets/images/avatar-4.jpg" alt="">
                                        </div>
                                        <div class="messages">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                                            </p>
                                            <span class="time"> 51 min </span>
                                        </div>
                                    </li>
                                    <li class="self">
                                        <div class="avatar">
                                            <img src="<?php echo base_url();?>assets/images/avatar-1.jpg" alt="">
                                        </div>
                                        <div class="messages">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                                            </p>
                                            <span class="time"> 37 mins </span>
                                        </div>
                                    </li>
                                    <li class="other">
                                        <div class="avatar">
                                            <img src="<?php echo base_url();?>assets/images/avatar-4.jpg" alt="">
                                        </div>
                                        <div class="messages">
                                            <p>
                                                Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt ut laoreet dolore magna aliquam erat volutpat.
                                            </p>
                                        </div>
                                    </li>
                                </ol>
                            </div>
                        </div>
                        <div class="tab-pane" id="notifications">
                            <div class="notifications">
                                <div class="pageslide-title">
                                    You have 11 notifications
                                </div>
                                <ul class="pageslide-list">
                                    <li>
                                        <a href="javascript:void(0)">
                                            <span class="label label-primary"><i class="fa fa-user"></i></span> <span class="message"> New user registration</span> <span class="time"> 1 min</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <span class="label label-success"><i class="fa fa-comment"></i></span> <span class="message"> New comment</span> <span class="time"> 7 min</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <span class="label label-success"><i class="fa fa-comment"></i></span> <span class="message"> New comment</span> <span class="time"> 8 min</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <span class="label label-success"><i class="fa fa-comment"></i></span> <span class="message"> New comment</span> <span class="time"> 16 min</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <span class="label label-primary"><i class="fa fa-user"></i></span> <span class="message"> New user registration</span> <span class="time"> 36 min</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="javascript:void(0)">
                                            <span class="label label-warning"><i class="fa fa-shopping-cart"></i></span> <span class="message"> 2 items sold</span> <span class="time"> 1 hour</span>
                                        </a>
                                    </li>
                                    <li class="warning">
                                        <a href="javascript:void(0)">
                                            <span class="label label-danger"><i class="fa fa-user"></i></span> <span class="message"> User deleted account</span> <span class="time"> 2 hour</span>
                                        </a>
                                    </li>
                                </ul>
                                <div class="view-all">
                                    <a href="javascript:void(0)">
                                        See all notifications <i class="fa fa-arrow-circle-o-right"></i>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="settings">
                            <h5 class="sidebar-title">General Settings</h5>
                            <ul class="media-list">
                                <li class="media">
                                    <div class="checkbox sidebar-content">
                                        <label>
                                            <input type="checkbox" value="" class="green" checked="checked">
                                            Enable Notifications
                                        </label>
                                    </div>
                                </li>
                                <li class="media">
                                    <div class="checkbox sidebar-content">
                                        <label>
                                            <input type="checkbox" value="" class="green" checked="checked">
                                            Show your E-mail
                                        </label>
                                    </div>
                                </li>
                                <li class="media">
                                    <div class="checkbox sidebar-content">
                                        <label>
                                            <input type="checkbox" value="" class="green">
                                            Show Offline Users
                                        </label>
                                    </div>
                                </li>
                                <li class="media">
                                    <div class="checkbox sidebar-content">
                                        <label>
                                            <input type="checkbox" value="" class="green" checked="checked">
                                            E-mail Alerts
                                        </label>
                                    </div>
                                </li>
                                <li class="media">
                                    <div class="checkbox sidebar-content">
                                        <label>
                                            <input type="checkbox" value="" class="green">
                                            SMS Alerts
                                        </label>
                                    </div>
                                </li>
                            </ul>
                            <div class="sidebar-content">
                                <button class="btn btn-success">
                                    <i class="icon-settings"></i> Save Changes
                                </button>
                            </div>
                        </div>
                    </div>
                    <div class="hidden-xs" id="style_selector">
                        <div id="style_selector_container">
                            <div class="pageslide-title">
                                Style Selector
                            </div>
                            <div class="box-title">
                                Choose Your Layout Style
                            </div>
                            <div class="input-box">
                                <div class="input">
                                    <select name="layout" class="form-control">
                                        <option value="default">Wide</option><option value="boxed">Boxed</option>
                                    </select>
                                </div>
                            </div>
                            <div class="box-title">
                                Choose Your Header Style
                            </div>
                            <div class="input-box">
                                <div class="input">
                                    <select name="header" class="form-control">
                                        <option value="fixed">Fixed</option><option value="default">Default</option>
                                    </select>
                                </div>
                            </div>
                            <div class="box-title">
                                Choose Your Sidebar Style
                            </div>
                            <div class="input-box">
                                <div class="input">
                                    <select name="sidebar" class="form-control">
                                        <option value="fixed">Fixed</option><option value="default">Default</option>
                                    </select>
                                </div>
                            </div>
                            <div class="box-title">
                                Choose Your Footer Style
                            </div>
                            <div class="input-box">
                                <div class="input">
                                    <select name="footer" class="form-control">
                                        <option value="default">Default</option><option value="fixed">Fixed</option>
                                    </select>
                                </div>
                            </div>
                            <div class="box-title">
                                10 Predefined Color Schemes
                            </div>
                            <div class="images icons-color">
                                <a href="#" id="default"><img src="<?php echo base_url();?>assets/images/color-1.png" alt="" class="active"></a>
                                <a href="#" id="style2"><img src="<?php echo base_url();?>assets/images/color-2.png" alt=""></a>
                                <a href="#" id="style3"><img src="<?php echo base_url();?>assets/images/color-3.png" alt=""></a>
                                <a href="#" id="style4"><img src="<?php echo base_url();?>assets/images/color-4.png" alt=""></a>
                                <a href="#" id="style5"><img src="<?php echo base_url();?>assets/images/color-5.png" alt=""></a>
                                <a href="#" id="style6"><img src="<?php echo base_url();?>assets/images/color-6.png" alt=""></a>
                                <a href="#" id="style7"><img src="<?php echo base_url();?>assets/images/color-7.png" alt=""></a>
                                <a href="#" id="style8"><img src="<?php echo base_url();?>assets/images/color-8.png" alt=""></a>
                                <a href="#" id="style9"><img src="<?php echo base_url();?>assets/images/color-9.png" alt=""></a>
                                <a href="#" id="style10"><img src="<?php echo base_url();?>assets/images/color-10.png" alt=""></a>
                            </div>
                            <div class="box-title">
                                Backgrounds for Boxed Version
                            </div>
                            <div class="images boxed-patterns">
                                <a href="#" id="bg_style_1"><img src="<?php echo base_url();?>assets/images/bg.png" alt=""></a>
                                <a href="#" id="bg_style_2"><img src="<?php echo base_url();?>assets/images/bg_2.png" alt=""></a>
                                <a href="#" id="bg_style_3"><img src="<?php echo base_url();?>assets/images/bg_3.png" alt=""></a>
                                <a href="#" id="bg_style_4"><img src="<?php echo base_url();?>assets/images/bg_4.png" alt=""></a>
                                <a href="#" id="bg_style_5"><img src="<?php echo base_url();?>assets/images/bg_5.png" alt=""></a>
                            </div>
                            <div class="style-options">
                                <a href="#" class="clear_style">
                                    Clear Styles
                                </a>
                                <a href="#" class="save_style">
                                    Save Styles
                                </a>
                            </div>
                        </div>
                        <div class="style-toggle open"></div>
                    </div>
                </div>
            </div>
            <!-- end: PAGESLIDE RIGHT -->
            <!-- start: MAIN CONTAINER -->
            <div class="main-container inner">
                <!-- start: PAGE -->
                <div class="main-content">
                    <div class="container">


                        <?php echo $admin_mid_content;?>




                    </div>
                    <div class="subviews">
                        <div class="subviews-container"></div>
                    </div>
                </div>
                <!-- end: PAGE -->
            </div>
            <!-- end: MAIN CONTAINER -->
            <!-- start: FOOTER -->
            <footer class="inner">
                <div class="footer-inner">
                    <div class="pull-left">
                        2015 &copy; Mustafizur Rahman.
                    </div>
                    <div class="pull-right">
                        <span class="go-top"><i class="fa fa-chevron-up"></i></span>
                    </div>
                </div>
            </footer>
            <!-- end: FOOTER -->           
        </div>
        <!-- start: MAIN JAVASCRIPTS -->
        <!--[if lt IE 9]>
        <script src="<?php echo base_url();?>assets/plugins/respond.min.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/excanvas.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url();?>assets/plugins/jQuery/jquery-1.11.1.min.js"></script>
        <![endif]-->
        <!--[if gte IE 9]><!-->
        <script src="<?php echo base_url();?>assets/plugins/jQuery/jquery-2.1.1.min.js"></script>
        <!--<![endif]-->
        <script src="<?php echo base_url();?>assets/plugins/jquery-ui/jquery-ui-1.10.2.custom.min.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/bootstrap/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/blockUI/jquery.blockUI.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/iCheck/jquery.icheck.min.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/moment/min/moment.min.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/perfect-scrollbar/src/jquery.mousewheel.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/perfect-scrollbar/src/perfect-scrollbar.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/bootbox/bootbox.min.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/jquery.scrollTo/jquery.scrollTo.min.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/ScrollToFixed/jquery-scrolltofixed-min.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/jquery.appear/jquery.appear.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/jquery-cookie/jquery.cookie.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/velocity/jquery.velocity.min.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/TouchSwipe/jquery.touchSwipe.min.js"></script>
        <!-- end: MAIN JAVASCRIPTS -->
        <!-- start: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
        <script src="<?php echo base_url();?>assets/plugins/owl-carousel/owl-carousel/owl.carousel.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/jquery-mockjax/jquery.mockjax.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/toastr/toastr.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/bootstrap-modal/js/bootstrap-modal.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/bootstrap-modal/js/bootstrap-modalmanager.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/fullcalendar/fullcalendar/fullcalendar.min.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/bootstrap-switch/dist/js/bootstrap-switch.min.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/bootstrap-fileupload/bootstrap-fileupload.min.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/DataTables/media/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/DataTables/media/js/DT_bootstrap.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/truncate/jquery.truncate.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/summernote/dist/summernote.min.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
        <script src="<?php echo base_url();?>assets/js/subview.js"></script>
        <script src="<?php echo base_url();?>assets/js/subview-examples.js"></script>
        <!-- end: JAVASCRIPTS REQUIRED FOR SUBVIEW CONTENTS -->
        <!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <script src="<?php echo base_url();?>assets/plugins/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/nvd3/lib/d3.v3.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/nvd3/nv.d3.min.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/nvd3/src/models/historicalBar.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/nvd3/src/models/historicalBarChart.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/nvd3/src/models/stackedArea.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/nvd3/src/models/stackedAreaChart.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/jquery.sparkline/jquery.sparkline.js"></script>
        <script src="<?php echo base_url();?>assets/plugins/easy-pie-chart/dist/jquery.easypiechart.min.js"></script>
        <script src="<?php echo base_url();?>assets/js/index.js"></script>
        <!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
        <!-- start: CORE JAVASCRIPTS  -->
        <script src="<?php echo base_url();?>assets/js/main.js"></script>
        <!-- end: CORE JAVASCRIPTS  -->
        <script>
            jQuery(document).ready(function() {
                Main.init();
                SVExamples.init();
                Index.init();
            });
        </script>
    </body>
    <!-- end: BODY -->

    <!-- Mirrored from www.cliptheme.com/demo/rapido/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 20 Jan 2015 13:56:09 GMT -->
</html>