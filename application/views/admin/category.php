<!-- start: PAGE -->
<div class="main-content">

    <div class="container">

        <!-- start: BREADCRUMB -->
        <div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li>
                        <a href="">
                            <?php echo $title; ?>
                        </a>
                    </li>
                    <li class="active">
                        Basic Tables
                    </li>
                </ol>
            </div>
        </div>
        <!-- end: BREADCRUMB -->
        <!-- start: PAGE CONTENT -->
        <div class="row">
            <div class="col-md-3">
                <!-- start: BASIC TABLE PANEL -->
                <div class="panel panel-white">
                    <div class="panel-heading">
                        <h4 class="panel-title"><span class="text-bold"><?php echo $title; ?></span></h4>

                    </div>
                    <div class="panel-body">


                        <div class="form-group">
                            <label for="form-field-select-1">
                               Category
                            </label>
                            
                            
                            <select id="form-field-select-1" class="form-control" name="category" onchange="check_category(this.value)">
                                
                                <option value="">Select</option>
                            <?php 
                            foreach($all_category as $v_info )
                            {
                            ?>
                                <option value="<?php echo $v_info->category_id?>"><?php echo $v_info->category_name?></option>
                            <?php 
                            }
                            ?>
                            </select>
                            
                            
                            
                            <label for="form-field-select-1">
                               Sub category
                            </label>
                            
                            
                            <select id="subcategory" class="form-control" name="subcategory">
                               <option value="">--Select--</option>
                                
                            </select>
                      </div>
                       
                    </div>
                </div>
                <!-- end: BASIC TABLE PANEL -->
            </div>
        </div>

        <!-- end: PAGE CONTENT-->
    </div>

</div>
<!-- end: PAGE -->

<script type="text/javascript">

//Create a boolean variable to check for a valid Internet Explorer instance.
var xmlhttp = false;
//Check if we are using IE.
try {
//If the Javascript version is greater than 5.
xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
//alert ("You are using Microsoft Internet Explorer.");
} catch (e) {
//If not, then use the older active x object.
try {
//If we are using Internet Explorer.
xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
//alert ("You are using Microsoft Internet Explorer");
} catch (E) {
//Else we must be using a non-IE browser.
xmlhttp = false;
}
}
//If we are using a non-IE browser, create a javascript instance of the object.
if (!xmlhttp && typeof XMLHttpRequest != 'undefined') {
xmlhttp = new XMLHttpRequest();
//alert ("You are not using Microsoft Internet Explorer");
}

function check_category(value)
{
    var serverpage="<?php echo base_url();?>administrator/subcategory/"+value;
    
    //window.alert(serverpage);
    xmlhttp.open("GET", serverpage, true);
    xmlhttp.onreadystatechange = function()
    {
        //alert(xmlhttp.readystate);
        //alert(xmlhttp.status);
        
        if (xmlhttp.readyState === 4 && xmlhttp.status === 200)
        {            
            //alert(xmlhttp.status);
          document.getElementById("subcategory").innerHTML = xmlhttp.responseText;
        }
    }
    xmlhttp.send(null);
}
</script>
    