<!-- start: PAGE -->
<div class="main-content">
    
    <div class="container">
       
        <!-- start: BREADCRUMB -->
        <div class="row">
            <div class="col-md-12">
                <ol class="breadcrumb">
                    <li>
                        <a href="#">
                            Dashboard
                        </a>
                    </li>
                    <li class="active">
                        
                    </li>
                </ol>
            </div>
        </div>
        <!-- end: BREADCRUMB -->
        <!-- start: PAGE CONTENT -->
        <div class="row">
            <div class="col-md-12">
                <!-- start: BASIC TABLE PANEL -->
                <div class="panel panel-white">
                    <div class="panel-heading">
                        <h4 class="panel-title"><span class="text-bold"><?php echo $title?></span></h4>
                      
                    </div>
                    <div class="panel-body">
                        
                    
                    </div>
                </div>
                <!-- end: BASIC TABLE PANEL -->
            </div>
        </div>
      
        <!-- end: PAGE CONTENT-->
    </div>
    
</div>
<!-- end: PAGE -->