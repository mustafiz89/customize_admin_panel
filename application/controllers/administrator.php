<?php 
session_start();
if ( ! defined('BASEPATH')) exit('No direct script access allowed');


class Administrator extends CI_Controller {    
    
    public function __construct() {
        parent::__construct();
        $admin_id=$this->session->userdata('admin_id');
        if($admin_id==null)
        {
            redirect('login','refresh');
        }
    }

	
	public function index()
	{
            $data=array();
            $data['title']='Dashboard'; 
            $data['admin_mid_content']=$this->load->view('admin/dashboard',$data,true);
            $this->load->view('admin/admin_master',$data);
           
	}
        
        public function logout()
        {
            $this->session->unset_userdata('admin_id');
            $this->session->unset_userdata('admin_name');
            $sdata=array();
            $sdata['message']='You are successfully Logout';
            $this->session->set_userdata($sdata);
            redirect('login','refresh');
            
        }
        
         public function dashboard()
        {
            $data=array();
            $data['title']='Dashboard';
            $data['admin_mid_content']=$this->load->view('admin/dashboard',$data,true);
            $this->load->view('admin/admin_master',$data);
        }
        public function table()
        {
            $data=array();
            $data['title']='Table';
            $data['admin_mid_content']=$this->load->view('admin/table',$data,true);
            $this->load->view('admin/admin_master',$data);
        }
        
        public function category()
        {
            $data=array();
            $data['title']='Category Test';
            $data['all_category']=$this->administrator_model->select_all_category();                
            $data['admin_mid_content']=$this->load->view('admin/category',$data,true);
            $this->load->view('admin/admin_master',$data);
        }
        
        public function subcategory($category_id)
        {
 
            $result=$this->administrator_model->select_subcategory_by_id($category_id);            
                      
            echo '<option value="">--Select--</option>';
            foreach($result as $v_result)
            {
                
                echo '<option value='.$v_result->subcategory_id.'>'.$v_result->subcategory_name.'</option>';
            }
           
            
        }
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */
?>