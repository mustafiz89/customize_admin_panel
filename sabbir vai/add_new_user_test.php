<?php echo $head; ?>

<body ng-app="mainApp">
    <div  ng-controller="userscontrol"  class="container" >
        <?php echo $menu; ?>

        <div ng-repeat="u in users" class="jumbotron">
            <h2>Add New User</h2>
            
            <?php echo $kk=  "{{u.ug_id}}"; ?>
            <form ng-submit="submit()"  onSubmit="if (!confirm('Are You Sure?')) {
                                return false;
                            }">
                
                <div class="form-group">
                    
                    <input type="hidden" name="user_id" value="{{u.user_id}} >
                    <label for="exampleInputEmail1">User Name</label>
                    <input type="text" name="user_name"  required="required" class="form-control" id="exampleInputEmail1" value="{{u.user_name}}">
                    
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">Password</label>
                    <input type="text" name="password"  required="required" value="{{u.password}}" class="form-control" id="exampleInputEmail1" >
                </div>

                <div class="form-group">
                    <label for="exampleInputEmail1">User Status</label>
                    <input type="text" name="user_status"  required="required" value="{{u.user_status}}" class="form-control" id="exampleInputEmail1" >
                </div>
                
                
                <div class="form-group">
                            <label for="exampleInputEmail1">User Group</label>
                            <select class="form-control" name="user_group" required="required">
                                <?php foreach ($user_group as $hhh) { ?>                                   
                                    <option value="<?php echo $hhh->ug_id ;?>" <?php if ($hhh->ug_id == $kk) { echo ' selected="selected"'; }  ?>    ><?php echo $hhh->user_type;?></option>          
                                <?php } ?>
                            </select>
                        </div>
                <button type="submit" class="btn btn-default">Submit</button>
            </form>
            
            
            
            <div id="result" style="color: red; "></div>
            
            
            <script>
    
                var mainApp = angular.module("mainApp", []);
                
                
                
                mainApp.controller("userscontrol", function($scope, $http) {
                var baseUrl = '<?= base_url() ?>';
                $scope.users = [];        
                function getUsers() {                
                        $http.get(baseUrl+'dashboard/get_user/<?=$this->uri->segment(3)?>')
                                .success(function (data) {
                                    console.log(data);
                                    $scope.users = data.users;
                        })
                        .error(function (data) {
                            console.log(data);
                            alert("There was a problem. Please try again later.");
                        });
                    }
                getUsers();
                
                $scope.submit = function(){
                    event.preventDefault();
                    $("#result").html('');
                    var values = $(this).serialize();
                    
                    $.ajax({
                    url: "http://localhost/google/cibl/dashboard/user_modify",
                    type: "post",
                    data: values,
                        success: function(){
                            alert("success");
//                            $("#result").html('User Modifyed');
                        },
                        error:function(){
                            alert("failure");
//                            $("#result").html('There is error while submit');
                        }
                    });
                  };
              });    
            
            
            
//            $("#myform").submit(function(event) {
//
//                event.preventDefault();
//                $("#result").html('');
//                var values = $(this).serialize();
//
//                $.ajax({
//                    url: "http://localhost/google/cibl/dashboard/user_modify",
//                    type: "post",
//                    data: values,
//                    success: function(){
////                        alert("success");
//                        $("#result").html('User Modifyed');
//                    },
//                    error:function(){
////                        alert("failure");
//                        $("#result").html('There is error while submit');
//                    }
//                });
//            });


            </script>    
        </div>
    </div>
</body>
</html>