<?php session_start();
if(!defined('BASEPATH'))exit('No direct script access allowed');
/**
 * Description of administrator
 *
 * @author Md. Sabbir Hossen
 */
class Administrator extends CI_Controller {
    //put your code here
    public function __construct() {
        parent::__construct();
        $this->load->helper('text');
        $admin_id=$this->session->userdata('admin_id');
        if($admin_id==NULL)
        {
            redirect('lib_admin','refresh');
        }
        date_default_timezone_set("Asia/Dhaka");
    }
    public function index(){
        $data=array();
        //$data['total_visitor']=  count($this->welcome_model->count_site_visitor());
        $data['admin_content']=  $this->load->view('administrator/admin_home_content',$data, TRUE);
        $this->load->view('administrator/admin_master', $data);
    }
    public function view_profile($admin_id)
    {
        $data=array();
        $data['admin_info']=  $this->administrator_model->view_admin_profile($admin_id);
        $data['admin_content']=  $this->load->view('administrator/admin_profile', $data, TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function edit_admin_profile($admin_id)
    {
        $data=array();
        $data['admin_info']=  $this->administrator_model->view_admin_profile($admin_id);
        $data['admin_content']=  $this->load->view('administrator/edit_admin_profile', $data, TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function update_admin_info()
    {
        
        $data=array();
        $admin_id=  $this->input->post('admin_id',TRUE);
        $del_img_adm=$this->administrator_model->view_admin_profile($admin_id);
        
        $data['first_name']=  $this->input->post('first_name',TRUE);
        $data['last_name']=  $this->input->post('last_name',TRUE);
        $data['birthday']=  $this->input->post('birthday',TRUE);
        $data['profession']=  $this->input->post('profession',TRUE);
        $data['user_name']=  $this->input->post('user_name',TRUE);
        $data['email']=  $this->input->post('email',TRUE);
        $data['mobile_number']=  $this->input->post('mobile_number',TRUE);
        $data['address']=  $this->input->post('admin_address',TRUE);
        $data['about_admin']=  $this->input->post('about_admin',TRUE);
        
        $fdata=array();
        /*
         * start upload/ update user profile picture with resizing
         */
        $config['upload_path'] = 'images/profile_picture/';
	$config['allowed_types'] = 'gif|jpg|png|jpeg';
	$config['overwrite'] = FALSE;
	$config['remove_spaces'] = true;
	$config['max_size']= '130';// in KB

	$this->load->library('upload', $config);

	if ( ! $this->upload->do_upload('profile_image'))
	{
            if ("You did not select a file to upload." != $this->upload->display_errors('','')) {
                // in here we know they DID provide a file
                // but it failed upload, display error
                $error=$this->upload->display_errors();
                $edata=array();
                $edata['message']=$error;
                $this->session->set_userdata($edata);
                redirect("administrator/edit_admin_profile/$admin_id",'refresh');
            }
            else {
                // here we failed b/c they did not provide a file to upload
                // fail silently, or message user, up to you
            }
	}
	else
	{
            //Image Resizing
            $config['source_image'] = $this->upload->upload_path.$this->upload->file_name;
            $config['maintain_ratio'] = TRUE;
            $config['width'] = 220;
            $config['height'] = 180;

            $this->load->library('image_lib', $config);

            if ( ! $this->image_lib->resize())
            {
                $error = array('error' => $this->upload->display_errors());
                $this->session->set_flashdata('error',$error['error']);
                redirect('administrator/view_profile/'.$admin_id,'refresh');
            }

            $fdata=$this->upload->data();
            $data['photo']=$config['upload_path'].$fdata['file_name'];
            $del_image='images/profile_images/'.$del_img_adm->admin_photo;
            unlink($del_image);
	}
        /*
         * ENd of uplaod/ update user profile picture
         */
        $this->administrator_model->update_admin_profile_by_id($admin_id, $data);
        
        $sdata=array();
        $sdata['message']='You have successfully Update your information';
        $this->session->set_userdata($sdata);
        redirect('administrator/view_profile/'.$admin_id);  
    }
    public function change_password($admin_id)
    {
        $data=array();
        $data['change_password']=  $this->administrator_model->view_admin_profile($admin_id);
        $data['admin_content']=  $this->load->view('administrator/change_password',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function update_password()
    {
        $data=array();
        $admin_id=  $this->input->post('admin_id',TRUE);
        $data['password']=  md5($this->input->post('password',TRUE));
        $this->administrator_model->update_password_by_id($admin_id,$data);
        
        $sdata=array();
        $sdata['message']='You have successfully Change Password';
        $this->session->set_userdata($sdata);
        redirect('administrator/change_password/'.$admin_id);
    }
     public function add_book_category(){
        $data=array();
        $data['admin_content']=  $this->load->view('administrator/add_book_category',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function save_book_category()
    {
        $data=array();
        $data['category_name']=  $this->input->post('book_category_name',TRUE);
        $data['category_status']=  $this->input->post('book_category_status',TRUE);
        $this->administrator_model->save_a_book_category($data);
        $sdata=array();
        $sdata['message']='You have successfully add a Books Category';
        $this->session->set_userdata($sdata);
        redirect('administrator/add_book_category','refresh');
    }
    public function manage_book_category()
    {
        $data=array();
        $data['book_category']=  $this->administrator_model->select_all_book_category();
        $data['admin_content']=  $this->load->view('administrator/manage_book_category',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function unpublished_book_category($category_id)
    {
        $this->administrator_model->unpublish_book_category_by_id($category_id);
        redirect('administrator/manage_book_category');
    }
    public function publish_book_category($category_id)
    {
        $this->administrator_model->publish_book_category_by_id($category_id);
        redirect('administrator/manage_book_category');
    }
    public function edit_book_category($category_id)
    {
        $data=array();
        $data['book_category']= $this->administrator_model->select_book_category_by_id($category_id);
        $data['admin_content']=  $this->load->view('administrator/edit_book_category',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function update_book_category()
    {
        $data=array();
        $category_id= $this->input->post('category_id',TRUE);
        $data['category_name']=  $this->input->post('category_name',TRUE);
        $this->administrator_model->update_book_category_by_id($category_id,$data);
        $sdata=array();
        $sdata['message']='You have successfully update the your book category';
        $this->session->set_userdata($sdata);
        redirect('administrator/manage_book_category');
    }
    public function delete_book_category($category_id)
    {
        $this->administrator_model->delete_book_category_by_id($category_id);
        redirect('administrator/manage_book_category');
    }
    //----- Book  Sub  categoty code start here
    public function add_book_subcategory(){
        $data=array();
        $data['category']=  $this->administrator_model->select_published_books_category();
        $data['admin_content']=  $this->load->view('administrator/add_book_subcategory',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function save_book_subcategory()
    {
        $data=array();
        $data['subcategory_name']=  $this->input->post('book_subcategory_name',TRUE);
        $data['category_id']=  $this->input->post('category_id',TRUE);
        $data['subcategory_status']=  $this->input->post('book_category_status',TRUE);
        $this->administrator_model->save_a_book_subcategory($data);
        $sdata=array();
        $sdata['message']='You have successfully add a Books Sub-Category';
        $this->session->set_userdata($sdata);
        redirect('administrator/add_book_subcategory','refresh');
    }
    public function manage_book_subcategory()
    {
        $data=array();
        $data['sub_category']=  $this->administrator_model->select_all_book_subcategory();
        $data['admin_content']=  $this->load->view('administrator/manage_book_subcategory',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function unpublished_book_subcategory($subcategory_id)
    {
        $this->administrator_model->unpublish_book_subcategory_by_id($subcategory_id);
        redirect('administrator/manage_book_subcategory');
    }
    public function publish_book_subcategory($subcategory_id)
    {
        $this->administrator_model->publish_book_subcategory_by_id($subcategory_id);
        redirect('administrator/manage_book_subcategory');
    }
    public function edit_book_subcategory($subcategory_id)
    {
        $data=array();
        $data['category']=  $this->administrator_model->select_published_books_category();
        $data['book_subcategory']= $this->administrator_model->select_book_subcategory_by_id($subcategory_id);
        $data['admin_content']=  $this->load->view('administrator/edit_book_subcategory',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function update_book_subcategory()
    {
        $data=array();
        $subcategory_id= $this->input->post('subcategory_id',TRUE);
        $data['subcategory_name']=  $this->input->post('subcategory_name',TRUE);
        $data['category_id']=  $this->input->post('category_id',TRUE);
        $this->administrator_model->update_book_subcategory_by_id($subcategory_id,$data);
        $sdata=array();
        $sdata['message']='You have successfully update the your book sub-category';
        $this->session->set_userdata($sdata);
        redirect('administrator/manage_book_subcategory');
    }
    public function delete_book_subcategory($subcategory_id)
    {
        $this->administrator_model->delete_book_subcategory_by_id($subcategory_id);
        redirect('administrator/manage_book_subcategory');
    }
    //----- Book sub category code end here
    //----- Upload/ add Books item code start here
    public function add_book_item(){
        $data=array();
        $data['book_category']=  $this->administrator_model->select_published_books_category();
        $data['admin_content']=  $this->load->view('administrator/add_book_item',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function getvalue(){
        $cat_id=$_POST['category_id'];
        echo $cat_id;
        exit();
        $qry = $this->administrator_model->select_subcategory_by_categoryid($cat_id);
        print_r($qry);
        $sql = mysql_query($qry);
        while($row = mysql_fetch_array($sql)){
        echo "<option value=".$row['subcategory_id'].">".$row['subcategory_name']."</option>";
        //echo "<option value=".$row['subcategory_id'];.">". $row['category_name'];."</option>";
        }
    }

    public function save_book_item()
    {
        $data=array();
        $data['subcategory_name']=  $this->input->post('book_subcategory_name',TRUE);
        $data['category_id']=  $this->input->post('category_id',TRUE);
        $data['subcategory_status']=  $this->input->post('book_category_status',TRUE);
        $this->administrator_model->save_a_book_subcategory($data);
        $sdata=array();
        $sdata['message']='You have successfully add a Books Item';
        $this->session->set_userdata($sdata);
        redirect('administrator/add_book_item','refresh');
    }
    public function manage_book_item()
    {
        $data=array();
        $data['sub_category']=  $this->administrator_model->select_all_book_subcategory();
        $data['admin_content']=  $this->load->view('administrator/manage_book_item',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function unpublished_book_item($subcategory_id)
    {
        $this->administrator_model->unpublish_book_subcategory_by_id($subcategory_id);
        redirect('administrator/manage_book_subcategory');
    }
    public function publish_book_item($subcategory_id)
    {
        $this->administrator_model->publish_book_subcategory_by_id($subcategory_id);
        redirect('administrator/manage_book_subcategory');
    }
    public function edit_book_item($subcategory_id)
    {
        $data=array();
        $data['category']=  $this->administrator_model->select_published_books_category();
        $data['book_subcategory']= $this->administrator_model->select_book_subcategory_by_id($subcategory_id);
        $data['admin_content']=  $this->load->view('administrator/edit_book_subcategory',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function update_book_item()
    {
        $data=array();
        $subcategory_id= $this->input->post('subcategory_id',TRUE);
        $data['subcategory_name']=  $this->input->post('subcategory_name',TRUE);
        $data['category_id']=  $this->input->post('category_id',TRUE);
        $this->administrator_model->update_book_subcategory_by_id($subcategory_id,$data);
        $sdata=array();
        $sdata['message']='You have successfully update the your book sub-category';
        $this->session->set_userdata($sdata);
        redirect('administrator/manage_book_subcategory');
    }
    public function delete_book_item($subcategory_id)
    {
        $this->administrator_model->delete_book_subcategory_by_id($subcategory_id);
        redirect('administrator/manage_book_subcategory');
    }
    //----- Upload / add Books item  code end here
   
    
    
    
    
    
    public function add_portfolio_image(){
        $data=array();
        $data['portfolio_category']=  $this->welcome_model->select_published_portfolio_category();
        $data['admin_content']=  $this->load->view('administrator/add_portfolio_image',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    /*public function save_portfolio_image()
    {
        $savedata=array();
        $savedata['portfolio_image_name']=  $this->input->post('portfolio_image_name',TRUE);
        $savedata['portfolio_image_short_description']=  $this->input->post('portfolio_image_short_description',TRUE);
        $savedata['portfolio_category_id']=  $this->input->post('portfolio_category_id',TRUE);
        $savedata['portfolio_image_status']=  $this->input->post('portfolio_image_status',TRUE);
        /*
         * Start of file upload
         /
        $name_array = array();
        $count = count($_FILES['userfile']['size']);
        foreach($_FILES as $key=>$value)
        for($s=0; $s<=$count-1; $s++) {
        $_FILES['userfile']['name']=$value['name'][$s];
        $_FILES['userfile']['type']    = $value['type'][$s];
        $_FILES['userfile']['tmp_name'] = $value['tmp_name'][$s];
        $_FILES['userfile']['error']       = $value['error'][$s];
        $_FILES['userfile']['size']    = $value['size'][$s];  
        $config['upload_path'] = 'images/portfolio/';
        $config['allowed_types'] = 'gif|jpg|png';
        $config['max_size']	= '200';
        $config['max_width']  = '924';
        $config['max_height']  = '688';
        $this->load->library('upload', $config);

        if(!$this->upload->do_upload('userfile'))
        {
            $error=$this->upload->display_errors();
            $sdata=array();
            $sdata['message']=$error;
            $this->session->set_userdata($sdata);
            redirect('administrator/add_portfolio_image','refresh');
        }
        else
        {
            $data = $this->upload->data();
            $name_array[] = $data['file_name'];
        }
        
        }
        $names=  $name_array;
        $savedata['portfolio_image_path']=$config['upload_path'].$names[0];
        $savedata['portfolio_image_thumb']=$config['upload_path'].$names[1];
        $this->administrator_model->save_a_portfolio_image($savedata);
        $sdata=array();
        $sdata['message']='You Have Successfully Add a Portfolio Image ';
        $this->session->set_userdata($sdata);
        redirect('administrator/add_portfolio_image','refresh');
        //redirect('administrator/add_portfolio_image','refresh');
        
    }*/
    public function save_portfolio_image()
    {
        $data=array();
        $data['portfolio_image_name']=  $this->input->post('portfolio_image_name',TRUE);
        $data['portfolio_image_short_description']=  $this->input->post('portfolio_image_short_description',TRUE);
        $data['portfolio_category_id']=  $this->input->post('portfolio_category_id',TRUE);
        $data['portfolio_link']=  $this->input->post('portfolio_link',TRUE);
        $data['portfolio_image_status']=  $this->input->post('portfolio_image_status',TRUE);
        /*
         * Start of file upload
         */
        
        $config['upload_path']='images/portfolio/';
        $config['allowed_types']='jpeg|jpg|gif|png';
        $config['max_size']='200';
        $config['max_width']='900';
        $config['max_height']='900';
        $config['remove_spaces'] = true;
        
        $fdata=array();
        $error='';
        $this->load->library('upload',$config);
        if(!$this->upload->do_upload('portfolio_image'))
        {
            $error=$this->upload->display_errors();
            $edata=array();
            $edata['message']=$error;
            $this->session->set_userdata($edata);
        }
        else
        {
            $fdata=$this->upload->data();
            $config1= array();
            $config1['image_library'] = 'gd2';
            $config1['source_image'] = $fdata['full_path'];
            $config1['new_image'] = 'images/portfolio/thumb/';
            $config1['create_thumb'] = TRUE;
            $config1['thumb_marker'] = '';
            $config1['maintain_ratio'] = TRUE;
            $config1['width'] = 220;
            $config1['height'] = 180;
            $this->load->library('image_lib', $config1);
            if( !$this->image_lib->resize() ){
                echo $this->image_lib->display_errors();
            }
            
            $data['portfolio_image_path']=$config['upload_path'].$fdata['file_name'];
            $data['portfolio_image_thumb']=$config1['new_image'].$fdata['file_name'];
            $this->administrator_model->save_a_portfolio_image($data);
            $sdata=array();
            $sdata['message']='You Have Successfully Add a Portfolio Image ';
            $this->session->set_userdata($sdata);
        }
        /*
         * End of file upload
         */
        
        redirect('administrator/add_portfolio_image','refresh');
    }
    public function manage_portfolio_image()
    {
        $data=array();
        $data['portfolio_image']=  $this->administrator_model->select_all_portfolio_image();
        $data['admin_content']=  $this->load->view('administrator/manage_portfolio_image',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function unpublished_portfolio_image($portfolio_image_id)
    {
        $this->administrator_model->unpublish_portfolio_image_by_id($portfolio_image_id);
        redirect('administrator/manage_portfolio_image');
    }
    public function publish_portfolio_image($portfolio_image_id)
    {
        $this->administrator_model->publish_portfolio_image_by_id($portfolio_image_id);
        redirect('administrator/manage_portfolio_image');
    }
    public function edit_portfolio_image($portfolio_image_id)
    {
        $data=array();
        $data['portfolio_category']=  $this->welcome_model->select_published_portfolio_category();
        $data['portfolio_image']= $this->administrator_model->select_portfolio_image_by_id($portfolio_image_id);
        $data['admin_content']=  $this->load->view('administrator/edit_portfolio_image',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function update_portfolio_image()
    {
        $data=array();
        $portfolio_image_id= $this->input->post('portfolio_image_id',TRUE);
        $data['portfolio_image_name']=  $this->input->post('portfolio_image_name',TRUE);
        $data['portfolio_category_id']=  $this->input->post('portfolio_category_id',TRUE);
        $data['portfolio_link']=  $this->input->post('portfolio_link',TRUE);
        $data['portfolio_image_short_description']=  $this->input->post('portfolio_image_short_description',TRUE);
        $del_portfolio_image=$this->administrator_model->portfolio_image_by_id($portfolio_image_id);
        /*
         * Start of file upload
         */
        $config['upload_path']='images/portfolio/';
        $config['allowed_types']='jpeg|jpg|gif|png';
        $config['max_size']='200';
        $config['max_width']='900';
        $config['max_height']='900';
        $config['remove_spaces'] = true;
        $fdata=array();
        $error='';
        $this->load->library('upload',$config);
        if(!$this->upload->do_upload('portfolio_image'))
        {
            if ("You did not select a file to upload." != $this->upload->display_errors('','')) {
                // in here we know they DID provide a file
                // but it failed upload, display error
                $error=$this->upload->display_errors();
                $edata=array();
                $edata['message']=$error;
                $this->session->set_userdata($edata);
                redirect("administrator/edit_portfolio_image/$portfolio_image_id");
            }
            else {
                // here we failed b/c they did not provide a file to upload
                // fail silently, or message user, up to you
            }
            
        }
        else
        {
            $fdata=$this->upload->data();
            $config1= array();
            $config1['image_library'] = 'gd2';
            $config1['source_image'] = $fdata['full_path'];
            $config1['new_image'] = 'images/portfolio/thumb/';
            $config1['create_thumb'] = TRUE;
            $config1['thumb_marker'] = '';
            $config1['maintain_ratio'] = TRUE;
            $config1['width'] = 220;
            $config1['height'] = 180;
            $this->load->library('image_lib', $config1);
            if( !$this->image_lib->resize() ){
                echo $this->image_lib->display_errors();
            }
            $data['portfolio_image_path']=$config['upload_path'].$fdata['file_name'];
            $data['portfolio_image_thumb']=$config1['new_image'].$fdata['file_name'];
            $del_image=$del_portfolio_image->portfolio_image_path;
            $del_image_thumb=$del_portfolio_image->portfolio_image_thumb;
            unlink($del_image);
            unlink($del_image_thumb);
        }
        /*
         * End of file upload
         */
        $this->administrator_model->update_portfolio_image_by_id($portfolio_image_id,$data);
        $sdata=array();
        $sdata['message']='You have successfully update the portfolio image information';
        $this->session->set_userdata($sdata);
        redirect('administrator/manage_portfolio_image');
    }
    public function delete_portfolio_image($portfolio_image_id)
    {
        $del_portfolio_image=$this->administrator_model->portfolio_image_by_id($portfolio_image_id);
        $del_image=$del_portfolio_image->portfolio_image_path;
        $del_image_thumb=$del_portfolio_image->portfolio_image_thumb;
        unlink($del_image);
        unlink($del_image_thumb);
        $this->administrator_model->delete_portfolio_image_by_id($portfolio_image_id);
        redirect('administrator/manage_portfolio_image');
    }
    
    
    
    
    public function add_profile_image(){
        $data=array();
        $data['admin_content']=  $this->load->view('administrator/add_profile_image',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    /*public function save_profile_image() ----------  by sahadat vai
    {
        $savedata=array();
        $savedata['profile_image_title']=  $this->input->post('profile_image_title',TRUE);
        $savedata['profile_image_status']=  $this->input->post('profile_image_status',TRUE);
        /*
         * Start of file upload
         /
        $name_array = array();
        $count = count($_FILES['userfile']['size']);
        foreach($_FILES as $key=>$value)
        for($s=0; $s<=$count-1; $s++) {
            $_FILES['userfile']['name']=$value['name'][$s];
            $_FILES['userfile']['type']    = $value['type'][$s];
            $_FILES['userfile']['tmp_name'] = $value['tmp_name'][$s];
            $_FILES['userfile']['error']       = $value['error'][$s];
            $_FILES['userfile']['size']    = $value['size'][$s];  
            $config['upload_path'] = 'images/img_profile/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size'] = '80';
            $config['max_width']  = '150';
            $config['max_height']  = '150';
            $this->load->library('upload', $config);

            if(!$this->upload->do_upload('userfile'))
            {
                $error=$this->upload->display_errors();
                $sdata=array();
                $sdata['message']=$error;
                $this->session->set_userdata($sdata);
                redirect('administrator/add_portfolio_image','refresh');
            }
            else
            {
                $data = $this->upload->data();
                $name_array[] = $data['file_name'];
            }
        
        }
        $extra['name'] = $name_array;
		$extra['upload_path'] = $config['upload_path'];
        
        $this->administrator_model->save_a_profile_image($savedata, $extra);
        $sdata=array();
        $sdata['message']='You Have Successfully Add Profile Image ';
        $this->session->set_userdata($sdata);
        redirect('administrator/add_profile_image','refresh');
        
    }*/
    
    public function save_profile_image()
    {
        $savedata=array();
        $savedata['profile_image_title']=  $this->input->post('profile_image_title',TRUE);
        $savedata['profile_image_status']=  $this->input->post('profile_image_status',TRUE);
        /*
         * Start of file upload
         */
        $name_array = array();
        $count = count($_FILES['userfile']['size']);
        foreach($_FILES as $key=>$value)
        for($s=0; $s<=$count-1; $s++) {
            $_FILES['userfile']['name']=$value['name'][$s];
            $_FILES['userfile']['type']    = $value['type'][$s];
            $_FILES['userfile']['tmp_name'] = $value['tmp_name'][$s];
            $_FILES['userfile']['error']       = $value['error'][$s];
            $_FILES['userfile']['size']    = $value['size'][$s];  
            $config['upload_path'] = 'images/img_profile/';
            $config['allowed_types'] = 'gif|jpg|png';
            $config['max_size']	= '80';
            $config['max_width']  = '170';
            $config['max_height']  = '170';
            $this->load->library('upload', $config);

            if(!$this->upload->do_upload('userfile'))
            {
                $error=$this->upload->display_errors();
                $sdata=array();
                $sdata['message']=$error;
                $this->session->set_userdata($sdata);
                redirect('administrator/add_profile_image','refresh');
            }
            else
            {
                $data = $this->upload->data();
                $t_data['profile_image_path'] = $config['upload_path'].$data['file_name'];
                $t_data['profile_image_status'] = $this->input->post('profile_image_status',TRUE);
                $t_data['profile_image_title'] = $this->input->post('profile_image_title',TRUE);
                $t_data['admin_id'] = 1;
                $this->db->insert('tbl_profile_image',$t_data);
                $sdata=array();
                $sdata['message']='You Have Successfully Add Profile Image ';
                $this->session->set_userdata($sdata);
            }
        
        }
        /*$names=  implode(',', $name_array);
        print_r($names);
        $this->load->database();
        $db_data = array('profile_image_id'=> NULL,
        'profile_image_path'=> $names);
        $this->db->insert('tbl_profile_image',$db_data);
        $savedata['profile_image_path']=$config['upload_path'].$names;
        $this->administrator_model->save_a_profile_image($savedata);
        $sdata=array();
        $sdata['message']='You Have Successfully Add Profile Image ';
        $this->session->set_userdata($sdata);*/
        
        redirect('administrator/add_profile_image','refresh');
        
    }
    public function manage_profile_image()
    {
        $data=array();
        $data['profile_image']=  $this->administrator_model->select_all_profile_image();
        $data['admin_content']=  $this->load->view('administrator/manage_profile_image',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function unpublished_profile_image($profile_image_id)
    {
        $this->administrator_model->unpublish_profile_image_by_id($profile_image_id);
        redirect('administrator/manage_profile_image');
    }
    public function publish_profile_image($profile_image_id)
    {
        $this->administrator_model->publish_profile_image_by_id($profile_image_id);
        redirect('administrator/manage_profile_image');
    }
    public function delete_profile_image($profile_image_id)
    {
        $del_profile_image=$this->administrator_model->profile_image_by_id($profile_image_id);
        $del_image=$del_profile_image->profile_image_path;
        unlink($del_image);
        $this->administrator_model->delete_profile_image_by_id($profile_image_id);
        redirect('administrator/manage_profile_image');
    }
    
    
    
    
    public function add_own_description(){
        $data=array();
        $data['admin_content']=  $this->load->view('administrator/add_own_description',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function save_own_description()
    {
        $data=array();
        $data['own_description_short_description']=  $this->input->post('own_description_short_description',TRUE);
        $data['own_description_brief_description']=  $this->input->post('own_description_brief_description',TRUE);
        $this->administrator_model->save_a_own_description($data);
        $sdata=array();
        $sdata['message']='You have successfully add Your Information';
        $this->session->set_userdata($sdata);
        redirect('administrator/add_own_description','refresh');
    }
    public function manage_own_description()
    {
        $data=array();
        $data['own_description']=  $this->administrator_model->select_all_own_description();
        $data['admin_content']=  $this->load->view('administrator/manage_own_description',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function edit_own_description($own_description_id)
    {
        $data=array();
        $data['own_description']= $this->administrator_model->select_own_description_by_id($own_description_id);
        $data['admin_content']=  $this->load->view('administrator/edit_own_description',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function update_own_description()
    {
        $data=array();
        $own_description_id= $this->input->post('own_description_id',TRUE);
        $data['own_description_short_description']=  $this->input->post('own_description_short_description',TRUE);
        $data['own_description_brief_description']=  $this->input->post('own_description_brief_description',TRUE);
        $this->administrator_model->update_own_description_by_id($own_description_id,$data);
        $sdata=array();
        $sdata['message']='You have successfully update the your own description';
        $this->session->set_userdata($sdata);
        redirect('administrator/manage_own_description');
    }
    public function add_experience(){
        $data=array();
        $data['admin_content']=  $this->load->view('administrator/add_experience',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function save_experience()
    {
        $data=array();
        $data['experience_position_title']=  $this->input->post('experience_position_title',TRUE);
        $data['experience_organization_name']=  $this->input->post('experience_organization_name',TRUE);
        $data['experience_description']=  $this->input->post('experience_description',TRUE);
        $data['experience_duration']=  $this->input->post('experience_duration',TRUE);
        $data['experience_status']=  $this->input->post('experience_status',TRUE);
        $this->administrator_model->save_a_experience($data);
        $sdata=array();
        $sdata['message']='You have successfully add An Experience Information';
        $this->session->set_userdata($sdata);
        redirect('administrator/add_experience','refresh');
    }
    public function manage_experience()
    {
        $data=array();
        $data['experience']=  $this->administrator_model->select_all_experience();
        $data['admin_content']=  $this->load->view('administrator/manage_experience',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function unpublished_experience($experience_id)
    {
        $this->administrator_model->unpublish_experience_by_id($experience_id);
        redirect('administrator/manage_experience');
    }
    public function publish_experience($experience_id)
    {
        $this->administrator_model->publish_experience_by_id($experience_id);
        redirect('administrator/manage_experience');
    }
    public function edit_experience($experience_id)
    {
        $data=array();
        $data['experience']= $this->administrator_model->select_experience_by_id($experience_id);
        $data['admin_content']=  $this->load->view('administrator/edit_experience',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function update_experience()
    {
        $data=array();
        $experience_id= $this->input->post('experience_id',TRUE);
        $data['experience_position_title']=  $this->input->post('experience_position_title',TRUE);
        $data['experience_organization_name']=  $this->input->post('experience_organization_name',TRUE);
        $data['experience_description']=  $this->input->post('experience_description',TRUE);
        $data['experience_duration']=  $this->input->post('experience_duration',TRUE);
        $this->administrator_model->update_experience_by_id($experience_id,$data);
        $sdata=array();
        $sdata['message']='You have successfully update the experience information';
        $this->session->set_userdata($sdata);
        redirect('administrator/manage_experience');
    }
    public function delete_experience($experience_id)
    {
        $this->administrator_model->delete_experience_by_id($experience_id);
        redirect('administrator/manage_experience');
    }
    public function add_education(){
        $data=array();
        $data['admin_content']=  $this->load->view('administrator/add_education',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function save_education()
    {
        $data=array();
        $data['education_title']=  $this->input->post('education_title',TRUE);
        $data['education_institute']=  $this->input->post('education_institute',TRUE);
        $data['education_duration']=  $this->input->post('education_duration',TRUE);
        $data['education_description']=  $this->input->post('education_description',TRUE);
        $data['education_status']=  $this->input->post('education_status',TRUE);
        $this->administrator_model->save_a_education($data);
        $sdata=array();
        $sdata['message']='You have successfully add an Education Information';
        $this->session->set_userdata($sdata);
        redirect('administrator/add_education','refresh');
    }
    public function manage_education()
    {
        $data=array();
        $data['education']=  $this->administrator_model->select_all_education();
        $data['admin_content']=  $this->load->view('administrator/manage_education',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function unpublished_education($education_id)
    {
        $this->administrator_model->unpublish_education_by_id($education_id);
        redirect('administrator/manage_education');
    }
    public function publish_education($education_id)
    {
        $this->administrator_model->publish_education_by_id($education_id);
        redirect('administrator/manage_education');
    }
    public function edit_education($education_id)
    {
        $data=array();
        $data['education']= $this->administrator_model->select_education_by_id($education_id);
        $data['admin_content']=  $this->load->view('administrator/edit_education',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function update_education()
    {
        $data=array();
        $education_id= $this->input->post('education_id',TRUE);
        $data['education_title']=  $this->input->post('education_title',TRUE);
        $data['education_institute']=  $this->input->post('education_institute',TRUE);
        $data['education_duration']=  $this->input->post('education_duration',TRUE);
        $data['education_description']=  $this->input->post('education_description',TRUE);
        $this->administrator_model->update_education_by_id($education_id,$data);
        $sdata=array();
        $sdata['message']='You have successfully update the education information';
        $this->session->set_userdata($sdata);
        redirect('administrator/manage_education');
    }
    public function delete_education($education_id)
    {
        $this->administrator_model->delete_education_by_id($education_id);
        redirect('administrator/manage_education');
    }
    public function add_course(){
        $data=array();
        $data['admin_content']=  $this->load->view('administrator/add_course',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function save_course()
    {
        $data=array();
        $data['course_title']=  $this->input->post('course_title',TRUE);
        $data['course_institute']=  $this->input->post('course_institute',TRUE);
        $data['course_duration']=  $this->input->post('course_duration',TRUE);
        $data['course_description']=  $this->input->post('course_description',TRUE);
        $data['course_status']=  $this->input->post('course_status',TRUE);
        $this->administrator_model->save_a_course($data);
        $sdata=array();
        $sdata['message']='You have successfully add an Course Information';
        $this->session->set_userdata($sdata);
        redirect('administrator/add_course','refresh');
    }
    public function manage_course()
    {
        $data=array();
        $data['course']=  $this->administrator_model->select_all_course();
        $data['admin_content']=  $this->load->view('administrator/manage_course',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function unpublished_course($course_id)
    {
        $this->administrator_model->unpublish_course_by_id($course_id);
        redirect('administrator/manage_course');
    }
    public function publish_course($course_id)
    {
        $this->administrator_model->publish_course_by_id($course_id);
        redirect('administrator/manage_course');
    }
    public function edit_course($course_id)
    {
        $data=array();
        $data['course']= $this->administrator_model->select_course_by_id($course_id);
        $data['admin_content']=  $this->load->view('administrator/edit_course',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function update_course()
    {
        $data=array();
        $course_id= $this->input->post('course_id',TRUE);
        $data['course_title']=  $this->input->post('course_title',TRUE);
        $data['course_institute']=  $this->input->post('course_institute',TRUE);
        $data['course_duration']=  $this->input->post('course_duration',TRUE);
        $data['course_description']=  $this->input->post('course_description',TRUE);
        $this->administrator_model->update_course_by_id($course_id,$data);
        $sdata=array();
        $sdata['message']='You have successfully update the course information';
        $this->session->set_userdata($sdata);
        redirect('administrator/manage_course');
    }
    public function delete_course($course_id)
    {
        $this->administrator_model->delete_course_by_id($course_id);
        redirect('administrator/manage_course');
    }
    public function add_reference(){
        $data=array();
        $data['admin_content']=  $this->load->view('administrator/add_reference',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function save_reference()
    {
        $data=array();
        $data['reference_name']=  $this->input->post('reference_name',TRUE);
        $data['reference_designation']=  $this->input->post('reference_designation',TRUE);
        $data['reference_country']=  $this->input->post('reference_country',TRUE);
        $data['reference_description']=  $this->input->post('reference_description',TRUE);
        $data['reference_status']=  $this->input->post('reference_status',TRUE);
        $this->administrator_model->save_a_reference($data);
        $sdata=array();
        $sdata['message']='You have successfully add a Reference Information';
        $this->session->set_userdata($sdata);
        redirect('administrator/add_reference','refresh');
    }
    public function manage_reference()
    {
        $data=array();
        $data['reference']=  $this->administrator_model->select_all_reference();
        $data['admin_content']=  $this->load->view('administrator/manage_reference',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function unpublished_reference($reference_id)
    {
        $this->administrator_model->unpublish_reference_by_id($reference_id);
        redirect('administrator/manage_reference');
    }
    public function publish_reference($reference_id)
    {
        $this->administrator_model->publish_reference_by_id($reference_id);
        redirect('administrator/manage_reference');
    }
    public function edit_reference($reference_id)
    {
        $data=array();
        $data['reference']= $this->administrator_model->select_reference_by_id($reference_id);
        $data['admin_content']=  $this->load->view('administrator/edit_reference',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function update_reference()
    {
        $data=array();
        $reference_id= $this->input->post('reference_id',TRUE);
        $data['reference_name']=  $this->input->post('reference_name',TRUE);
        $data['reference_designation']=  $this->input->post('reference_designation',TRUE);
        $data['reference_country']=  $this->input->post('reference_country',TRUE);
        $data['reference_description']=  $this->input->post('reference_description',TRUE);
        $this->administrator_model->update_reference_by_id($reference_id,$data);
        $sdata=array();
        $sdata['message']='You have successfully update the reference information';
        $this->session->set_userdata($sdata);
        redirect('administrator/manage_reference');
    }
    public function delete_reference($reference_id)
    {
        $this->administrator_model->delete_reference_by_id($reference_id);
        redirect('administrator/manage_reference');
    }
    public function add_blog_category(){
        $data=array();
        $data['admin_content']=  $this->load->view('administrator/add_blog_category',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function save_blog_category()
    {
        $data=array();
        $data['blog_category_name']=  $this->input->post('blog_category_name',TRUE);
        $data['blog_category_status']=  $this->input->post('blog_category_status',TRUE);
        $this->administrator_model->save_a_blog_category($data);
        $sdata=array();
        $sdata['message']='You have successfully add a Blog Category';
        $this->session->set_userdata($sdata);
        redirect('administrator/add_blog_category','refresh');
    }
    public function manage_blog_category()
    {
        $data=array();
        $data['blog_category']=  $this->administrator_model->select_all_blog_category();
        $data['admin_content']=  $this->load->view('administrator/manage_blog_category',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function unpublished_blog_category($blog_category_id)
    {
        $this->administrator_model->unpublish_blog_category_by_id($blog_category_id);
        redirect('administrator/manage_blog_category');
    }
    public function publish_blog_category($blog_category_id)
    {
        $this->administrator_model->publish_blog_category_by_id($blog_category_id);
        redirect('administrator/manage_blog_category');
    }
    public function edit_blog_category($blog_category_id)
    {
        $data=array();
        $data['blog_category']= $this->administrator_model->select_blog_category_by_id($blog_category_id);
        $data['admin_content']=  $this->load->view('administrator/edit_blog_category',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function update_blog_category()
    {
        $data=array();
        $blog_category_id= $this->input->post('blog_category_id',TRUE);
        $data['blog_category_name']=  $this->input->post('blog_category_name',TRUE);
        $this->administrator_model->update_blog_category_by_id($blog_category_id,$data);
        $sdata=array();
        $sdata['message']='You have successfully update the your blog category';
        $this->session->set_userdata($sdata);
        redirect('administrator/manage_blog_category');
    }
    public function delete_blog_category($blog_category_id)
    {
        $this->administrator_model->delete_blog_category_by_id($blog_category_id);
        redirect('administrator/manage_blog_category');
    }
    public function add_blog_item(){
        $data=array();
        $data['blog_category']=  $this->welcome_model->select_published_blog_category();
        $data['admin_content']=  $this->load->view('administrator/add_blog_item',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function save_blog_item()
    {
        date_default_timezone_set("Asia/Dhaka");
        $data=array();
        $data['blog_title']=  $this->input->post('blog_title',TRUE);
        $data['blog_category_id']=  $this->input->post('blog_category_id',TRUE);
        $data['blog_description']=  $this->input->post('blog_description',TRUE);
        $data['blog_status']=  $this->input->post('blog_status',TRUE);
        $data['blog_author']=  $this->input->post('blog_author',TRUE);
        $data['blog_date']=  date("Y-m-d", strtotime('-1 hours'));
        /*
         * Start of file upload
         */
        $config['upload_path']='images/blog/';
        $config['allowed_types']='jpeg|jpg|gif|png';
        $config['max_size']='80';
        $config['max_width']='900';
        $config['max_height']='350';
        $config['remove_spaces'] = true;
        $fdata=array();
        $error='';
        $this->load->library('upload',$config);
        if(!$this->upload->do_upload('blog_image'))
        {
            if ("You did not select a file to upload." != $this->upload->display_errors('','')) {
                // in here we know they DID provide a file --- but it failed upload, display error
                $error=$this->upload->display_errors();
                $edata=array();
                $edata['message']=$error;
                $this->session->set_userdata($edata);
                redirect("administrator/add_blog_item");
            }
            else {
                // here we failed b/c they did not provide a file to upload
                // fail silently, or message user, up to you
            }
        }
        else
        {
            $fdata=$this->upload->data();
            $data['blog_image']=$config['upload_path'].$fdata['file_name'];
        }
        $this->administrator_model->save_a_blog_item($data);
        $sdata=array();
        $sdata['message']='You Have Successfully Add a Blog Item ';
        $this->session->set_userdata($sdata);
        /*
         * End of file upload
         */
        redirect('administrator/add_blog_item','refresh');
    }
    public function manage_blog_item()
    {
        $data=array();
        $data['all_blog_item']=  $this->administrator_model->select_all_blog_item();
        $data['admin_content']=  $this->load->view('administrator/manage_blog_item',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function unpublished_blog_item($blog_id)
    {
        $this->administrator_model->unpublish_blog_item_by_id($blog_id);
        redirect('administrator/manage_blog_item');
    }
    public function publish_blog_item($blog_id)
    {
        $this->administrator_model->publish_blog_item_by_id($blog_id);
        redirect('administrator/manage_blog_item');
    }
    public function edit_blog_item($blog_id)
    {
        $data=array();
        $data['blog_item']= $this->administrator_model->select_blog_item_by_id($blog_id);
        $data['admin_content']=  $this->load->view('administrator/edit_blog_item',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function update_blog_item()
    {
        $data=array();
        $blog_id= $this->input->post('blog_id',TRUE);
        $data['blog_title']=  $this->input->post('blog_title',TRUE);
        $data['blog_description']=  $this->input->post('blog_description',TRUE);
        $del_blog_image=$this->administrator_model->blog_item_by_id($blog_id);
        /*
         * Start of file upload
         */
        $config['upload_path']='images/blog/';
        $config['allowed_types']='jpeg|jpg|gif|png';
        $config['max_size']='80';
        $config['max_width']='900';
        $config['max_height']='350';
        $config['remove_spaces'] = true;
        $fdata=array();
        $error='';
        $this->load->library('upload',$config);
        if(!$this->upload->do_upload('blog_image'))
        {
            if ("You did not select a file to upload." != $this->upload->display_errors('','')) {
                // in here we know they DID provide a file
                // but it failed upload, display error
                $error=$this->upload->display_errors();
                $edata=array();
                $edata['message']=$error;
                $this->session->set_userdata($edata);
                redirect("administrator/edit_blog_item/$blog_id");
            }
            else {
                // here we failed b/c they did not provide a file to upload
                // fail silently, or message user, up to you
            }
        }
        else
        {
            $fdata=$this->upload->data();
            $data['blog_image']=$config['upload_path'].$fdata['file_name'];
            $del_image=$del_blog_image->blog_image;
            unlink($del_image);
        }
        /*
         * End of file upload
         */
        $this->administrator_model->update_blog_item_by_id($blog_id,$data);
        $sdata=array();
        $sdata['message']='You Have Successfully Update the Blog Item Information';
        $this->session->set_userdata($sdata);
        redirect('administrator/manage_blog_item');
    }
    public function delete_blog_item($blog_id)
    {
        $this->administrator_model->delete_blog_item_by_id($blog_id);
        redirect('administrator/manage_blog_item');
    }
    public function add_skill_category(){
        $data=array();
        $data['admin_content']=  $this->load->view('administrator/add_skill_category',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function save_skill_category()
    {
        $data=array();
        $data['skill_category_name']=  $this->input->post('skill_category_name',TRUE);
        $data['skill_category_status']=  $this->input->post('skill_category_status',TRUE);
        $this->administrator_model->save_a_skill_category($data);
        $sdata=array();
        $sdata['message']='You have successfully add a Skill Category';
        $this->session->set_userdata($sdata);
        redirect('administrator/add_skill_category','refresh');
    }
    public function manage_skill_category()
    {
        $data=array();
        $data['skill_category']=  $this->administrator_model->select_all_skill_category();
        $data['admin_content']=  $this->load->view('administrator/manage_skill_category',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function unpublished_skill_category($skill_category_id)
    {
        $this->administrator_model->unpublish_skill_category_by_id($skill_category_id);
        redirect('administrator/manage_skill_category');
    }
    public function publish_skill_category($skill_category_id)
    {
        $this->administrator_model->publish_skill_category_by_id($skill_category_id);
        redirect('administrator/manage_skill_category');
    }
    public function edit_skill_category($skill_category_id)
    {
        $data=array();
        $data['skill_category']= $this->administrator_model->select_skill_category_by_id($skill_category_id);
        $data['admin_content']=  $this->load->view('administrator/edit_skill_category',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function update_skill_category()
    {
        $data=array();
        $skill_category_id= $this->input->post('skill_category_id',TRUE);
        $data['skill_category_name']=  $this->input->post('skill_category_name',TRUE);
        $this->administrator_model->update_skill_category_by_id($skill_category_id,$data);
        $sdata=array();
        $sdata['message']='You have successfully update the your skill category';
        $this->session->set_userdata($sdata);
        redirect('administrator/manage_skill_category');
    }
    public function delete_skill_category($skill_category_id)
    {
        $this->administrator_model->delete_skill_category_by_id($skill_category_id);
        redirect('administrator/manage_skill_category');
    }
    public function add_skill_item(){
        $data=array();
        $data['skill_category']=  $this->welcome_model->select_published_skill_category();
        $data['admin_content']=  $this->load->view('administrator/add_skill_item',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function save_skill_item()
    {
        $data=array();
        $data['skill_item_name']=  $this->input->post('skill_item_name',TRUE);
        $data['skill_category_id']=  $this->input->post('skill_category_id',TRUE);
        $data['skill_item_quality']=  $this->input->post('skill_item_quality',TRUE);
        $data['skill_item_status']=  $this->input->post('skill_item_status',TRUE);
        $this->administrator_model->save_a_skill($data);
        $sdata=array();
        $sdata['message']='You have successfully add a skill item';
        $this->session->set_userdata($sdata);
        redirect('administrator/add_skill_item','refresh');
    }
    public function manage_skill_item()
    {
        $data=array();
        $data['skill']=  $this->administrator_model->select_all_skill_item();
        $data['admin_content']=  $this->load->view('administrator/manage_skill_item',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function unpublished_skill_item($skill_item_id)
    {
        $this->administrator_model->unpublish_skill_by_id($skill_item_id);
        redirect('administrator/manage_skill_item');
    }
    public function publish_skill_item($skill_item_id)
    {
        $this->administrator_model->publish_skill_by_id($skill_item_id);
        redirect('administrator/manage_skill_item');
    }
    public function edit_skill_item($skill_item_id)
    {
        $data=array();
        $data['skill_category']=  $this->welcome_model->select_published_skill_category();
        $data['skill_item']= $this->administrator_model->select_skill_by_id($skill_item_id);
        $data['admin_content']=  $this->load->view('administrator/edit_skill_item',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function update_skill_item()
    {
        $data=array();
        $skill_item_id= $this->input->post('skill_item_id',TRUE);
        $data['skill_item_name']=  $this->input->post('skill_item_name',TRUE);
        $data['skill_category_id']=  $this->input->post('skill_category_id',TRUE);
        $data['skill_item_quality']=  $this->input->post('skill_item_quality',TRUE);
        $this->administrator_model->update_skill_by_id($skill_item_id,$data);
        $sdata=array();
        $sdata['message']='You have successfully update the skill item information';
        $this->session->set_userdata($sdata);
        redirect('administrator/manage_skill_item');
    }
    public function delete_skill_item($skill_item_id)
    {
        $this->administrator_model->delete_skill_by_id($skill_item_id);
        redirect('administrator/manage_skill_item');
    }
    public function add_service(){
        $data=array();
        $data['admin_content']=  $this->load->view('administrator/add_service',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function save_service()
    {
        $data=array();
        $data['service_name']=  $this->input->post('service_name',TRUE);
        $data['service_description']=  $this->input->post('service_description',TRUE);
        $data['service_status']=  $this->input->post('service_status',TRUE);
        
        $this->administrator_model->save_a_service($data);
        $sdata=array();
        $sdata['message']='You Have Successfully Add a Service ';
        $this->session->set_userdata($sdata);
        redirect('administrator/add_service','refresh');
    }
    public function manage_service()
    {
        $data=array();
        $data['all_service']=  $this->administrator_model->select_all_service();
        $data['admin_content']=  $this->load->view('administrator/manage_service',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function unpublished_service($service_id)
    {
        $this->administrator_model->unpublish_service_by_id($service_id);
        redirect('administrator/manage_service');
    }
    public function publish_service($service_id)
    {
        $this->administrator_model->publish_service_by_id($service_id);
        redirect('administrator/manage_service');
    }
    public function edit_service($service_id)
    {
        $data=array();
        $data['service_item']= $this->administrator_model->select_service_by_id($service_id);
        $data['admin_content']=  $this->load->view('administrator/edit_service',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function update_service()
    {
        $data=array();
        $service_id= $this->input->post('service_id',TRUE);
        $data['service_name']=  $this->input->post('service_name',TRUE);
        $data['service_description']=  $this->input->post('service_description',TRUE);
        
        $this->administrator_model->update_service_by_id($service_id,$data);
        $sdata=array();
        $sdata['message']='You Have Successfully Update the Service Information';
        $this->session->set_userdata($sdata);
        redirect('administrator/manage_service');
    }
    public function delete_service($service_id)
    {
        $this->administrator_model->delete_service_by_id($service_id);
        redirect('administrator/manage_service');
    }
    public function manage_comments()
    {
        $data=array();
        $data['all_comments']=  $this->administrator_model->select_all_comments();
        $data['admin_content']=  $this->load->view('administrator/manage_comments',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
    public function unpublished_comments($comments_id)
    {
        $this->administrator_model->unpublish_comments_by_id($comments_id);
        redirect('administrator/manage_comments');
    }
    public function publish_comments($comments_id)
    {
        $this->administrator_model->publish_comments_by_id($comments_id);
        redirect('administrator/manage_comments');
    }
    public function delete_comments($comments_id)
    {
        $this->administrator_model->delete_comments_by_id($comments_id);
        redirect('administrator/manage_comments');
    }
    
    public function manage_site_visitor()
    {
        $data=array();
        $data['total_visitor']=  $this->welcome_model->count_site_visitor();
        $data['admin_content']=  $this->load->view('administrator/manage_site_visitor',$data,TRUE);
        $this->load->view('administrator/admin_master',$data);
    }
 
    public function logout()
    {
        $this->session->unset_userdata('user_name');
        $this->session->unset_userdata('admin_id');
        $this->session->unset_userdata('email');
        $sdata=array();
        $sdata['message']='You Have Successfully Logout.';
        $this->session->set_userdata($sdata);
        redirect('lib_admin');
    }
}
?>