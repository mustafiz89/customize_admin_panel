<?php require_once(APPPATH.'views/administrator/config.php'); ?>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
    $(document).ready(function(){
    $('#category').change(function(){
    var category_id = $('#category').val();
    
    if(category_id != 0)
    {
    $.ajax({
    type:'post',
    url:'<?php echo base_url();?>"administrator/getvalue',
    data:{category_id:subcategory_id},
    cache:false,
    success: function(returndata){
    $('#subcategory').html(returndata);
    }
    });
    }
    })
    })
</script>
<div>
    <ul class="breadcrumb">
        <li>
            <a href="<?php echo base_url();?>administrator">Home</a> <span class="divider">/</span>
        </li>
        <li>
            <a href="#">Add Book Item</a>
        </li>
    </ul>
</div>

<div class="row-fluid sortable">
    <div class="box span12">
        <div class="box-header well" data-original-title>
            <h2><i class="icon-edit"></i> Add a Books Item</h2>
            <div class="box-icon">
                <a href="#" class="btn btn-setting btn-round"><i class="icon-cog"></i></a>
                <a href="#" class="btn btn-minimize btn-round"><i class="icon-chevron-up"></i></a>
                <a href="#" class="btn btn-close btn-round"><i class="icon-remove"></i></a>
            </div>
        </div>
        
        <div class="box-content">
                <?php
                    $msg=$this->session->userdata('message');
                    if($msg)
                    {
               ?>
                    <h3 class="alert">
               <?php
                        echo $msg.'</h3>';
                        $this->session->unset_userdata('message'); 
                    }
                ?>
              <form class="form-horizontal" method="post" action="<?php echo base_url();?>administrator/save_book_item" enctype="multipart/form-data"onsubmit="return validateStandard(this);">
                <fieldset>
                    <legend>Add a Book at Here</legend>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Book Name <span class="saddam">*</span></label>
                        <div class="controls">
                            <input type="text" name="book_name" class="span6 typeahead" id="typeahead" required data-provide="typeahead" data-items="4" >
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Category Name <span class="saddam">*</span></label>
                        <div class="controls">
                            <select id="category" name="category_id" err="Please Select a Category Name !" required exclude=" ">
                                <option value=" ">Select Category</option>
                                <?php $sql = mysql_query('SELECT * FROM `lib_category`'); ?>
                                <?php while($row = mysql_fetch_array($sql)){ ?>
                                <option value="<?php echo $row['category_id']; ?>"><?php echo $row['category_name']; ?></option>
                                <?php } ?>
                            </select>
                            <select id="subcategory" name="subcategory_id" class="selectbox">
                                <option value="">Select a sub-category</option>
                                <option></option>
                            </select>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Author Name<span class="saddam">*</span></label>
                        <div class="controls">
                            <input type="text" name="book_author" class="span6 typeahead" id="typeahead" required  data-provide="typeahead" data-items="4" >
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="textarea2">Book Short Description <span class="saddam">*</span></label>
                        <div class="controls">
                            <textarea class="cleditor" required name="book_short_description" id="textarea2" rows="3"></textarea>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Publication Year<span class="saddam"></span></label>
                        <div class="controls">
                            <input type="text" name="publication_year" class="span6 typeahead" id="typeahead" data-provide="typeahead" data-items="4" >
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Book Image <span class="saddam">*</span></label>
                        <div class="controls">
                            <!--<input type="file" multiple="" name="userfile[]" class="span6 typeahead" id="userfile" data-provide="typeahead" data-items="4" >-->
                            <input type="file" multiple="" name="book_image" class="span6 typeahead" id="userfile" data-provide="typeahead" data-items="4" >
                            <span class="saddam">MAX ( size=200 kb, width=900 px, height=900 px )</span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="typeahead">Select Book <span class="saddam">*</span></label>
                        <div class="controls">
                            <input type="file" multiple="" name="book_file" class="span6 typeahead" required id="userfile" data-provide="typeahead" data-items="4" >
                            <span class="saddam">Select book for publish and read </span>
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label">Publication Status</label> 
                        <div class="controls">
                            <label class="radio">
                                <input type="radio" name="book_status" id="optionsRadios1" value="1" checked="">
                                Publish
                            </label>
                            <div style="clear:both"></div>
                            <label class="radio">
                                <input type="radio" name="book_status" id="optionsRadios2" value="0">
                                Un-publish
                            </label>
                        </div>
                    </div>
                    <div class="form-actions">
                        <button type="submit" name="go" class="btn btn-primary">Save Book Item</button>
                        <button type="reset" class="btn">Reset</button>
                    </div>
                </fieldset>
            </form>   
        </div>
    </div><!--/span-->
</div><!--/row-->