<?php ob_start("ob_gzhandler"); ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
	<title>Admin Panel</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="A professional web developer's portfolio admin panel.">
	<meta name="author" content="Md Sabbir Hossen">

        <!-- The styles -->
        <link href="<?php echo base_url();?>css/bootstrap-cyborg.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-bottom: 40px;
            }
            .sidebar-nav {
                padding: 9px 0;
            }
            .dropdown-submenu{position:relative;}
            .dropdown-submenu>.dropdown-menu{top:0;left:100%;margin-top:0px;margin-left:-1px;-webkit-border-radius:0 6px 6px 6px;-moz-border-radius:0 6px 6px 6px;border-radius:0 6px 6px 6px;}
            .dropdown-submenu:hover>.dropdown-menu{display:block;}
            .dropdown-submenu>a:after{display:block;content:" ";float:right;width:0;height:0;border-color:transparent;border-style:solid;border-width:5px 0 5px 5px;border-left-color:#cccccc;margin-top:5px;margin-right:-10px;}
            .dropdown-submenu:hover>a:after{border-left-color:#ffffff;}
            .dropdown-submenu.pull-left{float:none;}.dropdown-submenu.pull-left>.dropdown-menu{left:-100%;margin-left:10px;-webkit-border-radius:6px 0 6px 6px;-moz-border-radius:6px 0 6px 6px;border-radius:6px 0 6px 6px;}
        </style>
        <link href="<?php echo base_url();?>css/bootstrap-responsive.css" rel="stylesheet">
        <link href="<?php echo base_url();?>css/charisma-app.css" rel="stylesheet">
        <link href="<?php echo base_url();?>css/jquery-ui-1.8.21.custom.css" rel="stylesheet">
        <link href='<?php echo base_url();?>css/fullcalendar.css' rel='stylesheet'>
        <link href='<?php echo base_url();?>css/fullcalendar.print.css' rel='stylesheet'  media='print'>
        <link href='<?php echo base_url();?>css/chosen.css' rel='stylesheet'>
        <link href='<?php echo base_url();?>css/uniform.default.css' rel='stylesheet'>
        <link href='<?php echo base_url();?>css/colorbox.css' rel='stylesheet'>
        <link href='<?php echo base_url();?>css/jquery.cleditor.css' rel='stylesheet'>
        <link href='<?php echo base_url();?>css/jquery.noty.css' rel='stylesheet'>
        <link href='<?php echo base_url();?>css/noty_theme_default.css' rel='stylesheet'>
        <link href='<?php echo base_url();?>css/elfinder.min.css' rel='stylesheet'>
        <link href='<?php echo base_url();?>css/elfinder.theme.css' rel='stylesheet'>
        <link href='<?php echo base_url();?>css/jquery.iphone.toggle.css' rel='stylesheet'>
        <link href='<?php echo base_url();?>css/opa-icons.css' rel='stylesheet'>
        <link href='<?php echo base_url();?>css/uploadify.css' rel='stylesheet'>
        
        <script type="text/javascript" src="<?php echo base_url(); ?>script/jsval.js"></script>
        <script type="text/javascript">
            function check_delete()
            {
                var check=confirm('Are You Sure to Delete This ???');
                if(check)
                    {
                        return true;
                    }
                else
                    {
                        return false;
                    }
            }
        </script>
        <!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
        <!--[if lt IE 9]>
          <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
        <![endif]-->

        <!-- The fav icon -->
        <link rel="shortcut icon" href="<?php echo base_url();?>images/favicon/favicon.ico">

    </head>

    <body>
        <!-- topbar starts -->
        <div class="navbar">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".top-nav.nav-collapse,.sidebar-nav.nav-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="<?php echo base_url();?>administrator"> <span>Sabbir's Website</span></a>

                    <!-- user dropdown starts -->
                    <div class="btn-group pull-right" >
                        <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                            <i class="icon-user"></i><span class="hidden-phone"> <?php echo $this->session->userdata['first_name'].' '.$this->session->userdata['last_name'].' '; ?></span>
                            <span class="caret"></span>
                        </a>
                        <ul class="dropdown-menu">
                            <li><a href="<?php echo base_url();?>administrator/view_profile/<?php echo $this->session->userdata['admin_id'];?>">Profile</a></li>
                            <li><a href="<?php echo base_url();?>administrator/change_password/<?php echo $this->session->userdata['admin_id'];?>">Change Password</a></li>
                            <li class="divider"></li>
                            <li><a href="<?php echo base_url();?>administrator/logout">Logout</a></li>
                        </ul>
                    </div>
                    <!-- user dropdown ends -->

                    <div class="top-nav nav-collapse">
                        <ul class="nav">
                            <li><a href="<?php echo base_url();?>" target="_blank">Visit Site</a></li>
                        </ul>
                    </div><!--/.nav-collapse -->
                </div>
            </div>
        </div>
        <!-- topbar ends -->
        <div class="container-fluid">
            <div class="row-fluid">

                <!-- left menu starts -->
                <div class="span2 main-menu-span">
                    <?php $this->load->view('administrator/admin_main_menu');?>
                </div><!--/span-->
                <!-- left menu ends -->

                <noscript>
                <div class="alert alert-block span10">
                    <h4 class="alert-heading">Warning!</h4>
                    <p>You need to have <a href="http://en.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> enabled to use this site.</p>
                </div>
                </noscript>
                
                <div id="content" class="span10">
                    
                    <?php echo $admin_content;?>
                    
                </div><!--/#content.span10-->
            </div><!--/fluid-row-->

            <hr>

            <div class="modal hide fade" id="myModal">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">×</button>
                    <h3>Settings</h3>
                </div>
                <div class="modal-body">
                    <p>Here settings can be configured...</p>
                </div>
                <div class="modal-footer">
                    <a href="#" class="btn" data-dismiss="modal">Close</a>
                    <a href="#" class="btn btn-primary">Save changes</a>
                </div>
            </div>

            <footer>
                <p class="pull-left">&copy; <a href="<?php echo base_url();?>">mdsabbir.com</a> 2012</p>
                <p class="pull-right">Powered by: <a href="<?php echo base_url();?>">Md. Sabbir Hossen</a></p>
            </footer>

        </div><!--/.fluid-container-->

        <!-- external javascript
        ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->

        <!-- jQuery -->
        <script src="<?php echo base_url();?>js/jquery-1.7.2.min.js"></script>
        <!-- jQuery UI -->
        <script src="<?php echo base_url();?>js/jquery-ui-1.8.21.custom.min.js"></script>
        <!-- transition / effect library -->
        <script src="<?php echo base_url();?>js/bootstrap-transition.js"></script>
        <!-- alert enhancer library -->
        <script src="<?php echo base_url();?>js/bootstrap-alert.js"></script>
        <!-- modal / dialog library -->
        <script src="<?php echo base_url();?>js/bootstrap-modal.js"></script>
        <!-- custom dropdown library -->
        <script src="<?php echo base_url();?>js/bootstrap-dropdown.js"></script>
        <!-- scrolspy library -->
        <script src="<?php echo base_url();?>js/bootstrap-scrollspy.js"></script>
        <!-- library for creating tabs -->
        <script src="<?php echo base_url();?>js/bootstrap-tab.js"></script>
        <!-- library for advanced tooltip -->
        <script src="<?php echo base_url();?>js/bootstrap-tooltip.js"></script>
        <!-- popover effect library -->
        <script src="<?php echo base_url();?>js/bootstrap-popover.js"></script>
        <!-- button enhancer library -->
        <script src="<?php echo base_url();?>js/bootstrap-button.js"></script>
        <!-- accordion library (optional, not used in demo) -->
        <script src="<?php echo base_url();?>js/bootstrap-collapse.js"></script>
        <!-- carousel slideshow library (optional, not used in demo) -->
        <script src="<?php echo base_url();?>js/bootstrap-carousel.js"></script>
        <!-- autocomplete library -->
        <script src="<?php echo base_url();?>js/bootstrap-typeahead.js"></script>
        <!-- tour library -->
        <script src="<?php echo base_url();?>js/bootstrap-tour.js"></script>
        <!-- library for cookie management -->
        <script src="<?php echo base_url();?>js/jquery.cookie.js"></script>
        <!-- calander plugin -->
        <script src='<?php echo base_url();?>js/fullcalendar.min.js'></script>
        <!-- data table plugin -->
        <script src='<?php echo base_url();?>js/jquery.dataTables.min.js'></script>

        <!-- chart libraries start -->
        <script src="<?php echo base_url();?>js/excanvas.js"></script>
        <script src="<?php echo base_url();?>js/jquery.flot.min.js"></script>
        <script src="<?php echo base_url();?>js/jquery.flot.pie.min.js"></script>
        <script src="<?php echo base_url();?>js/jquery.flot.stack.js"></script>
        <script src="<?php echo base_url();?>js/jquery.flot.resize.min.js"></script>
        <!-- chart libraries end -->

        <!-- select or dropdown enhancer -->
        <script src="<?php echo base_url();?>js/jquery.chosen.min.js"></script>
        <!-- checkbox, radio, and file input styler -->
        <script src="<?php echo base_url();?>js/jquery.uniform.min.js"></script>
        <!-- plugin for gallery image view -->
        <script src="<?php echo base_url();?>js/jquery.colorbox.min.js"></script>
        <!-- rich text editor library -->
        <script src="<?php echo base_url();?>js/jquery.cleditor.min.js"></script>
        <!-- notification plugin -->
        <script src="<?php echo base_url();?>js/jquery.noty.js"></script>
        <!-- file manager library -->
        <script src="<?php echo base_url();?>js/jquery.elfinder.min.js"></script>
        <!-- star rating plugin -->
        <script src="<?php echo base_url();?>js/jquery.raty.min.js"></script>
        <!-- for iOS style toggle switch -->
        <script src="<?php echo base_url();?>js/jquery.iphone.toggle.js"></script>
        <!-- autogrowing textarea plugin -->
        <script src="<?php echo base_url();?>js/jquery.autogrow-textarea.js"></script>
        <!-- multiple file upload plugin -->
        <script src="<?php echo base_url();?>js/jquery.uploadify-3.1.min.js"></script>
        <!-- history.js for cross-browser state change on ajax -->
        <script src="<?php echo base_url();?>js/jquery.history.js"></script>
        <!-- application script for Charisma demo -->
        <script src="<?php echo base_url();?>js/charisma.js"></script>
    </body>
</html>